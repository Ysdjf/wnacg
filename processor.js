const bookFetch = require('./book_fetch');
const URL = require('./baseurl');
const baseURL = URL.baseURL;

/**
 * @property {String}key need override the key for caching
 * @method load need override,
 * @method checkNew need override
 */
class MangaProcesser extends Processor {

    // The key for caching data
    get key() {
        return this.data.link;
    }

    /**
     * 
     * Start load pictures
     * 
     * @param {*} state The saved state.
     */
     async load(state) {
        try {
            let url = this.data.link;
            let res = await fetch(url, {
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
                },
                Referer: baseURL
            });
            let text = await res.text();
            let doc = HTMLParser.parse(text);
            let data = [];
            let script = doc.querySelector('script').textContent;
            let start = script.search('imglist');
            let end = script.search('喜歡紳士漫畫的同學請加入收藏哦！');
            let img_data = script.substring(start + 12, end + 17);
            let img_list = img_data.split('},{');
            for (let list of img_list) {
                if (img_list.indexOf(list) == img_list.length - 1) {
                    break;
                }
                data.push({
                    url: 'http:' + list.substring(list.search('//'), list.search('", webp') - 1),
                    headers: {
                        Referer: baseURL
                    }
                });
            }
            this.setData(data);
            this.save(true, state);
            this.loading = false;
        } catch (e) {
            console.log(`err ${e}\n${e.stack}`);
            this.loading = false;
        }
    }

    async fetch(url) {
        console.log(`request ${url}`);
        let res = await fetch(url, {
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
            },
            Referer: baseURL
        });
        let text = await res.text();
        return HTMLParser.parse(text);
    }

    // Called in `dispose`
    unload() {

    }

    // Check for new chapter
    async checkNew() {
        /**
         * @property {String}title The last chapter title.
         * @property {String}key The unique identifier of last chpater.
         */
        return {
            title: '',
            key: '',
        };
    }
}

module.exports = MangaProcesser;
