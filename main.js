const URL = require('./baseurl');
const baseURL = URL.baseURL;

class MainController extends Controller {

    load(data) {
        this.id = data.id;
        this.url = data.url;
        this.page = 0;

        var cached = this.readCache();
        let list;
        if (cached) {
            list = cached.items;
        } else {
            list = [];
        }

        this.data = {
            list: list,
            loading: false,
            hasMore: true
        };

        this.userAgent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0';

        if (cached) {
            let now = new Date().getTime();
            if (now - cached.time > 30 * 60 * 1000) {
                this.reload();
            }
        } else {
            this.reload();
        }

    }

    async onPressed(index) {
        await this.navigateTo('book', {
            data: this.data.list[index]
        });
    }

    onRefresh() {
        this.reload();
    }

    async onLoadMore() {
        this.setState(() => {
            this.data.loading = true;
        });
        try {
            let page = this.page + 1;
            let url = this.makeURL(page);
            let res = await fetch(url, {
                headers: {
                    'User-Agent': this.userAgent,
                },
            });
            let text = await res.text();
            this.page = page;
            let items = this.parseData(text, url);
    
            this.setState(()=>{
                for (let item of items) {
                    this.data.list.push(item);
                }
                this.data.loading = false;
                this.data.hasMore = true;
                console.log(`id: ${this.id} ${this.data.hasMore}`)
            });
        } catch (e) {
            showToast('没有更多了');
            this.data.hasMore = false;
            this.setState(()=>{
                this.data.loading = false;
            });
        }
        
    }

    makeURL(page) {
        return this.url.replace('page-1', `page-${page + 1}`);
    }

    async reload() {
        this.setState(() => {
            this.data.loading = true;
        });
        try {
            let url = this.makeURL(0);
            let res = await fetch(url, {
                headers: {
                    'User-Agent': this.userAgent,
                },
            });
            let text = await res.text();
            let items = this.parseData(text, url);
            this.page = 0;
            localStorage['cache_' + this.id] = JSON.stringify({
                time: new Date().getTime(),
                items: items,
            });
            this.setState(()=>{
                this.data.list = items;
                this.data.loading = false;
                this.data.hasMore = true;
            });
        } catch (e) {
            showToast(`${e}\n${e.stack}`);
            this.data.hasMore = false;
            this.setState(()=>{
                this.data.loading = false;
            });
        }
    }

    readCache() {
        let cache = localStorage['cache_' + this.id];
        if (cache) {
            let json = JSON.parse(cache);
            return json;
        }
    }

    parseData(text, url) {
        return this.parsePageData(text, url);
    }

    parsePageData(text, url) {
        const doc = HTMLParser.parse(text);
        let results = [];
        let boxes = doc.querySelectorAll('.gallary_item');
        for (let box of boxes) {
            let item = {};
            item.link = baseURL + box.querySelector('a').getAttribute('href');
            item.picture = 'http:' + box.querySelector('img').getAttribute('src');
            item.title = box.querySelector('a').getAttribute('title');
            item.subtitle = box.querySelector('.info_col').textContent.replace(/[\r\n]/g,"").split(" ").join("").replace(/[\r\t]/g,"");
            item.pictureHeaders = {
                Referer: url
            };
            results.push(item);
        }
        return results;
    }
}

module.exports = MainController;
