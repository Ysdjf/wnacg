const URL = require('./baseurl');
const baseURL = URL.baseURL;

class IndexController extends Controller {
    load() {
        this.data = {
            tabs: [
                {
                    "title": "更新",
                    "id": "update",
                    "url": baseURL + '/albums-index-page-1.html',
                },
                {
                    "title": "同人誌",
                    "id": "tongren",
                    "url": baseURL + "/albums-index-page-1-cate-5.html"
                },
                {
                    "title": "單行本",
                    "id": "book",
                    "url": baseURL + "/albums-index-page-1-cate-6.html"
                },
                {
                    "title": "雜誌&短篇",
                    "id": "mag",
                    "url": baseURL + "/albums-index-page-1-cate-7.html"
                },
                {
                    "title": "韓漫",
                    "id": "kor",
                    "url": baseURL + "/albums-index-page-1-cate-19.html"
                },
                {
                    "title": "寫真&Cosplay",
                    "id": "cos",
                    "url": baseURL + "/albums-index-page-1-cate-3.html"
                }, 
            ]
        };
    }
}

module.exports = IndexController;