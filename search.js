const URL = require('./baseurl');
const baseURL = URL.baseURL;
const Search_baseURL = baseURL + '/search/?q={0}&p={1}';
const Tags_baseURL = baseURL + '/albums-index-page-{1}-tag-{0}.html';

class SearchController extends Controller {

    load() {
        let str = localStorage['hints'];
        let hints = [];
        if (str) {
            let json = JSON.parse(str);
            if (json.push) {
                hints = json;
            }
        }
        this.data = {
            list: [],
            focus: false,
            hints: hints,
            text: '',
            loading: false,
            hasMore: true,
        };
        this.page_num = -1;
        this.page = 0;
    }

    makeURL(word, page) {
        if (word.search('tag:') != -1) {
            word = word.substring(4, word.length)
            return Tags_baseURL.replace('{0}', encodeURIComponent(word)).replace('{1}', page + 1);
        }
        return Search_baseURL.replace('{0}', encodeURIComponent(word)).replace('{1}', page + 1);
    }

    onSearchClicked() {
        this.findElement('input').submit();
    } 

    onTextChange(text) {
        this.data.text = text;
    }

    async onTextSubmit(text) {
        this.data.hasMore = true;
        this.page_num = -1;
        let hints = this.data.hints;
        if (text.length > 0) {
            if (hints.indexOf(text) < 0) {
                this.setState(()=>{
                    hints.unshift(text);
                    while (hints.length > 30) {
                        hints.pop();
                    }
    
                    localStorage['hints'] = JSON.stringify(hints);
                });
            }
            
            this.setState(()=>{
                this.data.loading = true;
            });
            try {
                let list = await this.request(this.makeURL(text, 0));
                this.key = text;
                this.page = 0;
                this.setState(()=>{
                    this.data.list = list;
                    this.data.loading = false;
                    this.data.hasMore = this.page_num == -1 || this.page < this.page_num - 1;
                });
            } catch(e) {
                showToast(`${e}\n${e.stack}`);
                this.setState(()=>{
                    this.data.loading = false;
                });
            }
        }
    }

    onTextFocus() {
        this.setState(()=>{
            this.data.focus = true;
        });
    }

    onTextBlur() {
        this.setState(()=>{
            this.data.focus = false;
        });
    }

    async onPressed(index) {
        await this.navigateTo('book', {
            data: this.data.list[index]
        });
    }

    onHintPressed(index) {
        let hint = this.data.hints[index];
        if (hint) {
            this.setState(()=>{
                this.data.text = hint;
                this.findElement('input').blur();
                this.onTextSubmit(hint);
            });
        }
    }

    async onRefresh() {
        this.data.hasMore = true;
        this.page_num = -1;
        let text = this.key;
        if (!text) return;
        try {
            let list = await this.request(this.makeURL(text, 0));
            this.page = 0;
            this.setState(()=>{
                this.data.list = list;
                this.data.loading = false;
                this.data.hasMore = this.page_num == -1 || this.page < this.page_num - 1;
            });
        } catch(e) {
            showToast(`${e}\n${e.stack}`);
            this.setState(()=>{
                this.data.loading = false;
            });
        }
    }

    async onLoadMore() {
        let page = this.page + 1;
        try {
            let list = await this.request(this.makeURL(this.key, page));
            this.page = page;
            this.setState(()=>{
                for (let item of list) {
                    this.data.list.push(item);
                }
                this.data.loading = false;
                this.data.hasMore = this.page_num == -1 || page < this.page_num - 1;
            });
        } catch(e) {
            this.hasMore = false;
            this.setState(()=>{
                this.data.loading = false;
            });
        }
    }

    async request(url) {
        let res = await fetch(url);
        let text = await res.text();
        let doc = HTMLParser.parse(text);
        if (url.search('-tag-') != -1) {
            let page_num_info = doc.querySelector('.paginator').querySelectorAll('a');
            if (page_num_info.length == 0) {
                this.page_num = 1;
            } else {
                this.page_num = parseInt(page_num_info[page_num_info.length - 2].textContent);
            }
        }
        let items = [];
        let list = doc.querySelectorAll('.gallary_item');
        for (let node of list) {
            let title = node.querySelector('img').getAttribute('alt');
            title = title.replaceAll('</em>', '');
            title = title.replaceAll('<em>', '');
            items.push({
                title: title,
                subtitle: node.querySelector('.info_col').textContent.replace(/[\r\n]/g,"").split(" ").join("").replace(/[\r\t]/g,""),
                link: baseURL + node.querySelector('a').getAttribute('href'),
                picture: 'http:' + node.querySelector('img').getAttribute('src'),
                pictureHeaders: {
                    Referer: url
                },
            });
        }
        return items;
    }
}

module.exports = SearchController;
