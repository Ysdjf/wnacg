const URL = require('./baseurl');
const baseURL = URL.baseURL;

class BookController extends Controller {

    /**
     * 
     * @property {String}key The unique identifier of the favorite item
     * @property {String}title The name of book
     * @property {String}subtitle The subtitle of book
     * @property {String}picture The cover of book.
     * @property {Object}data Data will be sent to book page.
     * @property {String}page The book page path.
     */
    get bookInfo() {
        return {
            key: this.url,
            title: this.data.title,
            subtitle: this.data.subtitle,
            picture: this.data.picture,
            page: 'book',
            data: {
                link: this.url,
                title: this.data.title,
                subtitle: this.data.subtitle,
                picture: this.data.picture,
                summary: this.data.summary,
            },
        };
    }

    async load(data) {
        this.data = {
            title: data.title,
            subtitle: data.subtitle,
            summary: data.summary,
            picture: data.picture,
            pictureHeaders: {
                Referer: baseURL
            },
            loading: false,
            editing: false,
            reverse: localStorage['reverse'] != 'false',
            list: [],
            images: [],
        };
        this.selected = [];
        this.page = 0;
        this.page_num = -1;
        this.url = data.link;
        
        /**
         * Add history record
         * 
         * Same as addFavorite with out lastData.
         */
         this.addHistory(this.bookInfo);
        
        let cached = localStorage[`book:${this.url}`];
        if (cached) {
            let data = JSON.parse(cached);
            for (let key in data) {
                if (key == 'time') continue;
                this.data[key] = data[key];
            }

            let now = new Date().getTime();
            if (now - (data.time || 0) > 30 * 60 * 1000) {
                this.reload();
            }
        } else {
            this.reload();
        }
        FavoritesManager.clearNew(this.url);
    }

    unload() {

    }

    onRefresh() {
        this.reload();
    }

    async reload() {
        this.setState(()=>{
            this.data.loading = true;
        });
        try {
            let url = this.url;
            let res = await fetch(url, {
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0',
                }
            });
            let text = await res.text();
            let get_page_url = url.replace('photos-index-aid-', 'photos-webp-aid-');
            let data = this.parseData(text, get_page_url);
            let now = new Date().getTime();
            data.time = now;
            localStorage[`book:${this.url}`] = JSON.stringify(data);
    
            this.setState(()=>{
                for (let key in data) {
                    if (key == 'time') continue;
                    this.data[key] = data[key];
                }
                this.data.loading = false;
            });
        } catch (e) {
            showToast(`${e}\n${e.stack}`);
            this.setState(()=>{
                this.data.loading = false;
            });
        }
    }

    async onLoadMore() {
        if (this.page_num == -1 || this.page < this.page_num - 1) {
            this.setState(() => {
                this.data.loading = true;
            });
            try {
                let page = this.page + 1;
                let url = this.url.replace('photos-index-aid-', `photos-index-page-${page + 1}-aid-`);
                let res = await fetch(url, {
                    headers: {
                        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0',
                    },
                });
                let text = await res.text();
                this.page = page;
                let items = this.get_img(text);
        
                this.setState(()=>{
                    for (let item of items) {
                        this.data.images.push(item);
                    }
                    this.data.loading = false;
                });
            } catch (e) {
                this.setState(()=>{
                    this.data.loading = false;
                });
            }
        }
    }

    get_img(text) {
        let doc = HTMLParser.parse(text);
        let data = [];
        let img_list = doc.querySelectorAll('.pic_box');
        for (let list of img_list) {
            let img_url = list.querySelector('img').getAttribute('src');
            if (img_url[3] == '/') {
                img_url = img_url.substring(2, img_url.length);
            }
            data.push('http:' + img_url);
        }
        return data;
    }

    parseData(text, url) {
        const doc = HTMLParser.parse(text);
        let tags = doc.querySelector('.addtags').querySelectorAll('a');
        let page_num_info = doc.querySelector('.paginator').querySelectorAll('a');
        if (page_num_info.length == 0) {
            this.page_num = 1;
        } else {
            this.page_num = parseInt(page_num_info[page_num_info.length - 2].textContent);
        }
        let label = doc.querySelectorAll('label');
        let img_url = doc.querySelectorAll('img')[1].getAttribute('src');
        if (img_url[3] == '/') {
            img_url = img_url.substring(2, img_url.length);
        }
        let dataTags = [];
        try {
            for (let tag of tags) {
                if (tag.textContent != '+TAG') {
                    dataTags.push({
                        title: tag.textContent,
                        links: baseURL + tag.getAttribute('href')
                    });
                }
            }
        } catch (e) {
            dataTags = [];
        }
        let item = {
            link: url,
            title: doc.querySelector('h2').textContent
        };
        let images = this.get_img(text);
        return {
            title: doc.querySelector('h2').textContent,
            subtitle: label[0].textContent + '  ' + label[1].textContent,
            picture: 'http:' + img_url,
            images: images,
            tags: dataTags,
            list: [item],
        };
    }

    onFavoritePressed() {
        this.setState(()=>{
            if (this.isFavarite()) {
                FavoritesManager.remove(this.url);
            } else {
                let last;
    
                /**
                 * Add to favorites list
                 * 
                 * The first argument see `bookinfo`
                 * 
                 * The second argument is optional
                 * @param {String}title The title of the last chapter
                 * @param {String}key The unique identifier of the last chapter
                 */
                if (this.data.list.length > 0) {
                    let data = this.data.list[this.data.list.length - 1];
                    last = {
                        title: data.title,
                        key: data.link,
                    };
                }
                this.addFavorite(this.bookInfo, last);
            }
        });
    }

    onDownloadPressed() {
        var data = this.data.list[0];
        this.addDownload([{
            key: data.link,
            title: this.data.title,
            link: this.url,
            picture: this.data.picture,
            subtitle: this.data.subtitle,
            data: data,
        }]);
    }

    onClearPressed() {
        this.selected = [];
        this.setState(()=>{
            this.data.editing = false;
        });
    }
    
    onCheckPressed() {
        let downloads = [];
        for (let idx of this.selected) {
            var data = this.data.list[idx];
            /**
             * Add to download queue
             * 
             * @param {String}key The unique identifier of the download item
             * @param {String}title The name of book
             * @param {String}subtitle The subtitle of book
             * @param {String}link The url of book, To group items with same book link.
             * @param {String}picture The cover of book.
             * @param {Object}data Data will be sent to processor load function.
             * @param {String*}data.title The title of chapter. 
             */
            downloads.push({
                key: data.link,
                title: this.data.title,
                link: this.url,
                picture: this.data.picture,
                subtitle: this.data.subtitle,
                data: data,
            });
        }
        this.addDownload(downloads);

        this.selected = [];
        this.setState(()=>{
            this.data.editing = false;
        });
    }

    onImagePressed(idx) {
        this.openBook({
            list: this.data.list,
            index: 0,
            page: idx,
        });
    }

    isSelected(index) {
        return this.selected.indexOf(index) >= 0;
    }

    onSourcePressed() {
        this.openBrowser(this.url);
    }

    onOrderSelected(value) {
        this.setState(()=>{
            this.data.reverse = value;
            localStorage['reverse'] = value.toString();
        });
    }

    isDownloaded(index) {
        let item = this.data.list[index];
        return DownloadManager.exist(item.link);
    }

    isFavarite() {
        return FavoritesManager.exist(this.url);
    }
}

module.exports = BookController;